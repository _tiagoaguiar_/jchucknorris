package co.tiagoaguiar.jchucknorris;

import android.app.Application;

import co.tiagoaguiar.jchucknorris.common.module.NetworkModule;
import co.tiagoaguiar.jchucknorris.jokes.categories.component.CategoryRepositoryComponent;
import co.tiagoaguiar.jchucknorris.jokes.categories.component.DaggerCategoryRepositoryComponent;
import co.tiagoaguiar.jchucknorris.jokes.random.component.DaggerJokeRepositoryComponent;
import co.tiagoaguiar.jchucknorris.jokes.random.component.JokeRepositoryComponent;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class ChuckNorrisApplication extends Application {

    private static final String BASE_URL = "https://api.chucknorris.io/";

    private CategoryRepositoryComponent mCategoryRepositoryComponent;
    private JokeRepositoryComponent mJokeRepositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mCategoryRepositoryComponent = DaggerCategoryRepositoryComponent.builder()
                .networkModule(new NetworkModule(BASE_URL))
                .build();

        mJokeRepositoryComponent = DaggerJokeRepositoryComponent.builder()
                .networkModule(new NetworkModule(BASE_URL))
                .build();
    }

    public CategoryRepositoryComponent getCategoryRepositoryComponent() {
        return mCategoryRepositoryComponent;
    }

    public JokeRepositoryComponent getJokeRepositoryComponent() {
        return mJokeRepositoryComponent;
    }

}