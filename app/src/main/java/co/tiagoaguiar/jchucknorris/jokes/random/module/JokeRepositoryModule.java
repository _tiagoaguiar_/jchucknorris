package co.tiagoaguiar.jchucknorris.jokes.random.module;

import javax.inject.Singleton;

import co.tiagoaguiar.jchucknorris.jokes.random.data.source.JokeDataSource;
import co.tiagoaguiar.jchucknorris.jokes.random.data.source.remote.JokeRemoteDataSource;
import dagger.Binds;
import dagger.Module;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Module
public abstract class JokeRepositoryModule {

    @Singleton
    @Binds
    public abstract JokeDataSource provideDataSource(JokeRemoteDataSource dataSource);

}