package co.tiagoaguiar.jchucknorris.jokes.categories.data.source;

import java.util.List;

import co.tiagoaguiar.jchucknorris.common.model.DataSource;
import co.tiagoaguiar.jchucknorris.jokes.categories.view.CategoryItem;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public interface CategoryDataSource {

    void findAll(ListCategoriesCallback callback);

    interface ListCategoriesCallback extends DataSource<List<String>, List<CategoryItem>> {
    }

}