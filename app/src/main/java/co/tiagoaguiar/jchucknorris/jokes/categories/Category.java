package co.tiagoaguiar.jchucknorris.jokes.categories;

import java.util.List;

import co.tiagoaguiar.jchucknorris.common.view.BaseView;
import co.tiagoaguiar.jchucknorris.jokes.categories.view.CategoryItem;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public interface Category {

    interface View extends BaseView<Presenter> {

        void showCategories(List<CategoryItem> categories);

        void showFailure(String message);
    }

    interface Presenter {

        void requestFindAll();

    }

}