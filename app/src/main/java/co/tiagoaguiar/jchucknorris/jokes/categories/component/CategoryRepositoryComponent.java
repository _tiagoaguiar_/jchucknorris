package co.tiagoaguiar.jchucknorris.jokes.categories.component;

import javax.inject.Singleton;

import co.tiagoaguiar.jchucknorris.common.module.NetworkModule;
import co.tiagoaguiar.jchucknorris.common.util.ViewScoped;
import co.tiagoaguiar.jchucknorris.jokes.categories.data.source.CategoryRepository;
import co.tiagoaguiar.jchucknorris.jokes.categories.module.CategoryRepositoryModule;
import dagger.Component;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Singleton
@Component(modules = {CategoryRepositoryModule.class, NetworkModule.class})
public interface CategoryRepositoryComponent {

    CategoryRepository getCategoryRepository();

}