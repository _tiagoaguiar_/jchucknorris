package co.tiagoaguiar.jchucknorris.jokes.random.data.source;

import co.tiagoaguiar.jchucknorris.common.model.DataSource;
import co.tiagoaguiar.jchucknorris.jokes.random.view.JokeItem;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public interface JokeDataSource {

    void findRandomBy(String category, JokeCallback callback);

    interface JokeCallback extends DataSource<Joke, JokeItem> {
    }

}