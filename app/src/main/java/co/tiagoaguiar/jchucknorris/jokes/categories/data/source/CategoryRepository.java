package co.tiagoaguiar.jchucknorris.jokes.categories.data.source;

import javax.inject.Inject;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class CategoryRepository implements CategoryDataSource {

    private final CategoryDataSource mDataSource;

    @Inject
    public CategoryRepository(CategoryDataSource dataSource) {
        mDataSource = dataSource;
    }

    @Override
    public void findAll(ListCategoriesCallback callback) {
        mDataSource.findAll(callback);
    }

}