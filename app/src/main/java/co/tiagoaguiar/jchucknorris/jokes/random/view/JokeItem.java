package co.tiagoaguiar.jchucknorris.jokes.random.view;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class JokeItem {

    private final String iconUrl;
    private final String url;
    private final String text;

    public JokeItem(String iconUrl, String url, String text) {
        this.iconUrl = iconUrl;
        this.url = url;
        this.text = text;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getUrl() {
        return url;
    }

    public String getText() {
        return text;
    }

}