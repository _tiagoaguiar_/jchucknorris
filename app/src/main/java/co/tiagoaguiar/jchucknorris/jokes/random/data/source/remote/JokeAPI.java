package co.tiagoaguiar.jchucknorris.jokes.random.data.source.remote;

import co.tiagoaguiar.jchucknorris.jokes.random.data.source.Joke;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public interface JokeAPI {

    @GET("jokes/random")
    Observable<Joke> findRandomBy(@Query("category") String category);

}