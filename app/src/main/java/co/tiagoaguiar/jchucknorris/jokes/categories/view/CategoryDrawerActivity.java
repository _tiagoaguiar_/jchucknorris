package co.tiagoaguiar.jchucknorris.jokes.categories.view;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;

import javax.inject.Inject;

import co.tiagoaguiar.jchucknorris.ChuckNorrisApplication;
import co.tiagoaguiar.jchucknorris.R;
import co.tiagoaguiar.jchucknorris.common.util.ActivityUtils;
import co.tiagoaguiar.jchucknorris.common.view.AbstractActivity;
import co.tiagoaguiar.jchucknorris.jokes.categories.component.CategoryRepositoryComponent;
import co.tiagoaguiar.jchucknorris.jokes.categories.component.DaggerCategoryComponent;
import co.tiagoaguiar.jchucknorris.jokes.categories.module.CategoryModule;
import co.tiagoaguiar.jchucknorris.jokes.categories.presenter.CategoryPresenter;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class CategoryDrawerActivity extends AbstractActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Inject
    CategoryPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupNavigationDrawer();

        CategoryFragment fragment = (CategoryFragment) ActivityUtils.addFragmentToActivity(
                this, CategoryFragment.class);

        CategoryRepositoryComponent repositoryComponent =
                ((ChuckNorrisApplication) getApplication()).getCategoryRepositoryComponent();

        DaggerCategoryComponent.builder()
                .categoryRepositoryComponent(repositoryComponent)
                .categoryModule(new CategoryModule(fragment))
                .build()
                .inject(this);
    }

     private void setupNavigationDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle( this, drawer, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        boolean selected = true;
        if (id == R.id.menu_about) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, R.style.Dialog);
            } else {
                builder = new AlertDialog.Builder(this);
            }
            builder.setTitle(getString(R.string.app_name))
                    .setMessage(getString(R.string.about_message))
                    .setPositiveButton(android.R.string.yes, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            selected = false;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return selected;
    }

    @Override
    protected Integer getMenuRes() {
        return null;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.act_drawer;
    }

}