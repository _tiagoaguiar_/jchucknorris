package co.tiagoaguiar.jchucknorris.jokes.random.presenter;

import javax.inject.Inject;

import co.tiagoaguiar.jchucknorris.jokes.random.Joke;
import co.tiagoaguiar.jchucknorris.jokes.random.data.source.JokeDataSource;
import co.tiagoaguiar.jchucknorris.jokes.random.data.source.JokeRepository;
import co.tiagoaguiar.jchucknorris.jokes.random.view.JokeItem;
import io.reactivex.disposables.Disposable;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class JokePresenter implements Joke.Presenter, JokeDataSource.JokeCallback {

    private final JokeRepository mRepository;
    private final Joke.View mView;

    @Inject
    public JokePresenter(JokeRepository repository, Joke.View view) {
        mRepository = repository;
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void onSubscribe(Disposable disposable) {
        mView.setDisposable(disposable);
        mView.showProgressBar();
    }

    @Override
    public void requestRandomBy(String category) {
        mRepository.findRandomBy(category, this);
    }

    @Override
    public void onSuccess(JokeItem jokeItem) {
        mView.showJoke(jokeItem);
    }

    @Override
    public void onError(String message) {
        mView.showFailure(message);
    }

    @Override
    public void onComplete() {
        mView.hideProgressBar();
    }

    @Override
    public JokeItem transform(co.tiagoaguiar.jchucknorris.jokes.random.data.source.Joke joke) {
        return new JokeItem(joke.getIconUrl(), joke.getUrl(), joke.getValue());
    }

}