package co.tiagoaguiar.jchucknorris.jokes.random.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import co.tiagoaguiar.jchucknorris.R;
import co.tiagoaguiar.jchucknorris.common.view.AbstractFragment;
import co.tiagoaguiar.jchucknorris.jokes.random.Joke;

/**
 * November, 17 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class JokeFragment extends AbstractFragment<Joke.Presenter> implements Joke.View, View.OnClickListener {

    private ImageView mImgIcon;
    private TextView mTxtJoke;

    private String mCategory;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mCategory = args.getString(JokeActivity.CATEGORY_KEY);
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        mImgIcon = view.findViewById(R.id.img_icon);
        mTxtJoke = view.findViewById(R.id.txt_joke);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDisposable.dispose();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPresenter.requestRandomBy(mCategory);
    }

    @Override
    public void onClick(View v) {
        mPresenter.requestRandomBy(mCategory);
    }

    @Override
    public void showJoke(JokeItem jokeItem) {
        mTxtJoke.setText(jokeItem.getText());
        Picasso.get().load(jokeItem.getIconUrl()).into(mImgIcon);
    }

    @Override
    public void showFailure(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.frag_joke;
    }

}