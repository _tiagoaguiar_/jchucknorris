package co.tiagoaguiar.jchucknorris.jokes.categories.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;
import com.xwray.groupie.OnItemClickListener;

import java.util.List;

import co.tiagoaguiar.jchucknorris.R;
import co.tiagoaguiar.jchucknorris.common.util.ActivityUtils;
import co.tiagoaguiar.jchucknorris.common.view.AbstractFragment;
import co.tiagoaguiar.jchucknorris.jokes.categories.Category;
import co.tiagoaguiar.jchucknorris.jokes.random.view.JokeActivity;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class CategoryFragment extends AbstractFragment<Category.Presenter> implements Category.View {

    private GroupAdapter mAdapter;

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        RecyclerView recyclerView = view.findViewById(R.id.rv_main);

        recyclerView.addItemDecoration(new DividerItemDecoration(mContext,
                DividerItemDecoration.VERTICAL));

        mAdapter = new GroupAdapter();
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(clickCategoryListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDisposable.dispose();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPresenter.requestFindAll();
    }

    @Override
    public void showCategories(List<CategoryItem> items) {
        mAdapter.addAll(items);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showFailure(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private OnItemClickListener clickCategoryListener = new OnItemClickListener() {
        @Override
        public void onItemClick(@NonNull Item item, @NonNull View view) {
            Intent intent = new Intent(getContext(), JokeActivity.class);
            CategoryItem categoryItem = (CategoryItem) item;

            intent.putExtra(JokeActivity.CATEGORY_KEY, categoryItem.getCategoryUrl());
            startActivity(intent);

            ActivityUtils.setAnimationOnOpen((Activity) mContext);
        }
    };

    @Override
    public int getLayoutRes() {
        return R.layout.frag_category;
    }

}