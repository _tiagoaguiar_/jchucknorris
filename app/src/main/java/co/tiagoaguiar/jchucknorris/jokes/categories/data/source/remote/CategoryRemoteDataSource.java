package co.tiagoaguiar.jchucknorris.jokes.categories.data.source.remote;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.tiagoaguiar.jchucknorris.jokes.categories.data.source.CategoryDataSource;
import co.tiagoaguiar.jchucknorris.jokes.categories.view.CategoryItem;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Singleton
public class CategoryRemoteDataSource implements CategoryDataSource {

    private final Retrofit mRetrofit;

    @Inject
    public CategoryRemoteDataSource(Retrofit mRetrofit) {
        this.mRetrofit = mRetrofit;
    }

    @Override
    public void findAll(final ListCategoriesCallback callback) {
        checkNotNull(callback);

        CategoryAPI categoryAPI = mRetrofit.create(CategoryAPI.class);
        categoryAPI.findAll()
                .subscribeOn(Schedulers.io())
                .flatMapIterable(new Function<List<String>, List<CategoryItem>>() {
                    @Override
                    public  List<CategoryItem> apply(List<String> categories) throws Exception {
                        return callback.transform(categories);
                    }
                })
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        callback.onComplete();
                    }
                })
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        callback.onSubscribe(disposable);
                    }
                })
                .subscribe(new Consumer<List<CategoryItem>>() {
                    @Override
                    public void accept(List<CategoryItem> categoryItems) throws Exception {
                        callback.onSuccess(categoryItems);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        callback.onError(throwable.getMessage());
                    }
                });

    }

}