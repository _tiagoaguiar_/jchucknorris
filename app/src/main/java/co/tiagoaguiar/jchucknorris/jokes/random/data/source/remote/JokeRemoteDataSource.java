package co.tiagoaguiar.jchucknorris.jokes.random.data.source.remote;

import javax.inject.Inject;

import co.tiagoaguiar.jchucknorris.jokes.random.data.source.Joke;
import co.tiagoaguiar.jchucknorris.jokes.random.data.source.JokeDataSource;
import co.tiagoaguiar.jchucknorris.jokes.random.view.JokeItem;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class JokeRemoteDataSource implements JokeDataSource {

    private final Retrofit mRetrofit;

    @Inject
    public JokeRemoteDataSource(Retrofit mRetrofit) {
        this.mRetrofit = mRetrofit;
    }

    @Override
    public void findRandomBy(String category, final JokeCallback callback) {
        checkNotNull(callback);

        mRetrofit.create(JokeAPI.class)
            .findRandomBy(category)
                .subscribeOn(Schedulers.io())
                .map(new Function<Joke, JokeItem>() {
                    @Override
                    public JokeItem apply(Joke joke) throws Exception {
                        return callback.transform(joke);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        callback.onComplete();
                    }
                })
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        callback.onSubscribe(disposable);
                    }
                })
                .subscribe(new Consumer<JokeItem>() {
                    @Override
                    public void accept(JokeItem jokeItem) throws Exception {
                        callback.onSuccess(jokeItem);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        callback.onError(throwable.getMessage());
                    }
                });
    }

}