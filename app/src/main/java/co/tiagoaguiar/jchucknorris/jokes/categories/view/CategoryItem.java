package co.tiagoaguiar.jchucknorris.jokes.categories.view;

import android.support.annotation.NonNull;
import android.widget.TextView;

import com.xwray.groupie.Item;
import com.xwray.groupie.ViewHolder;

import co.tiagoaguiar.jchucknorris.R;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class CategoryItem extends Item<ViewHolder> {

    private final String mCategory;
    private final String mCategoryUrl;
    private final int mBgColor;

    public CategoryItem(String category, String categoryUrl, int bgColor) {
        mCategory = category;
        mCategoryUrl = categoryUrl;
        mBgColor = bgColor;
    }

    @Override
    public void bind(@NonNull ViewHolder viewHolder, int position) {
        TextView txtCategory = viewHolder.itemView.findViewById(R.id.txt_category);
        txtCategory.setText(mCategory);
        viewHolder.itemView.setBackgroundColor(mBgColor);
    }

    public String getCategoryUrl() {
        return mCategoryUrl;
    }

    @Override
    public int getLayout() {
        return R.layout.card_category;
    }

}