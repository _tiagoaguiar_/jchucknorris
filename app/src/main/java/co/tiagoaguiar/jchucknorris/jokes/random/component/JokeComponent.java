package co.tiagoaguiar.jchucknorris.jokes.random.component;

import co.tiagoaguiar.jchucknorris.common.util.ViewScoped;
import co.tiagoaguiar.jchucknorris.jokes.random.module.JokeModule;
import co.tiagoaguiar.jchucknorris.jokes.random.view.JokeActivity;
import dagger.Component;

/**
 * November, 17 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@ViewScoped
@Component(dependencies = JokeRepositoryComponent.class, modules = JokeModule.class)
public interface JokeComponent {

    void inject(JokeActivity activity);

}