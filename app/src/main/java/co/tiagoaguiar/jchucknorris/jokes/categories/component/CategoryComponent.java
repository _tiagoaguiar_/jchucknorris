package co.tiagoaguiar.jchucknorris.jokes.categories.component;

import co.tiagoaguiar.jchucknorris.common.util.ViewScoped;
import co.tiagoaguiar.jchucknorris.jokes.categories.module.CategoryModule;
import co.tiagoaguiar.jchucknorris.jokes.categories.view.CategoryDrawerActivity;
import dagger.Component;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@ViewScoped
@Component(dependencies = CategoryRepositoryComponent.class, modules = CategoryModule.class)
public interface CategoryComponent {

    void inject(CategoryDrawerActivity activity);

}