package co.tiagoaguiar.jchucknorris.jokes.categories.module;

import javax.inject.Singleton;

import co.tiagoaguiar.jchucknorris.jokes.categories.data.source.CategoryDataSource;
import co.tiagoaguiar.jchucknorris.jokes.categories.data.source.remote.CategoryRemoteDataSource;
import dagger.Binds;
import dagger.Module;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Module
public abstract class CategoryRepositoryModule {

    @Singleton
    @Binds
    public abstract CategoryDataSource provideRemoteDataSource(CategoryRemoteDataSource dataSource);

}