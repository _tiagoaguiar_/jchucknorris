package co.tiagoaguiar.jchucknorris.jokes.categories.module;

import co.tiagoaguiar.jchucknorris.jokes.categories.Category;
import dagger.Module;
import dagger.Provides;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Module
public class CategoryModule {

    private final Category.View mView;

    public CategoryModule(Category.View view) {
        mView = view;
    }

    @Provides
    public Category.View provideView() {
        return mView;
    }

}