package co.tiagoaguiar.jchucknorris.jokes.categories.presenter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

import co.tiagoaguiar.jchucknorris.common.util.Colors;
import co.tiagoaguiar.jchucknorris.jokes.categories.Category;
import co.tiagoaguiar.jchucknorris.jokes.categories.data.source.CategoryDataSource;
import co.tiagoaguiar.jchucknorris.jokes.categories.data.source.CategoryRepository;
import co.tiagoaguiar.jchucknorris.jokes.categories.view.CategoryItem;
import io.reactivex.disposables.Disposable;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class CategoryPresenter implements Category.Presenter, CategoryDataSource.ListCategoriesCallback {

    private final CategoryRepository mRepository;
    private final Category.View mView;

    @Inject
    public CategoryPresenter(CategoryRepository repository, Category.View view) {
        mRepository = repository;
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void onSubscribe(Disposable disposable) {
        mView.setDisposable(disposable);
        mView.showProgressBar();
    }

    @Override
    public void requestFindAll() {
        mRepository.findAll(this);
    }

    @Override
    public void onSuccess(List<CategoryItem> categories) {
        mView.showCategories(categories);
    }

    @Override
    public void onError(String message) {
        mView.showFailure(message);
    }

    @Override
    public void onComplete() {
        mView.hideProgressBar();
    }

    @Override
    public List<CategoryItem> transform(List<String> categories) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return categories
                    .stream()
                    .map(new Function<String, CategoryItem>() {
                        @Override
                        public CategoryItem apply(String category) {
                            return new CategoryItem(category.toUpperCase(), category,
                                    Colors.randomColor());
                        }
                    })
                    .collect(Collectors.<CategoryItem>toList());
        }

        List<CategoryItem> items = new ArrayList<>();
        for (String category : categories) {
            items.add(new CategoryItem(category.toUpperCase(), category,
                    Colors.randomColor()));
        }
        return items;
    }

}