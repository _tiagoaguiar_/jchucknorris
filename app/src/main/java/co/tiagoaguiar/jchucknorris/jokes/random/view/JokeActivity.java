package co.tiagoaguiar.jchucknorris.jokes.random.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;

import javax.inject.Inject;

import co.tiagoaguiar.jchucknorris.ChuckNorrisApplication;
import co.tiagoaguiar.jchucknorris.R;
import co.tiagoaguiar.jchucknorris.common.util.ActivityUtils;
import co.tiagoaguiar.jchucknorris.common.view.AbstractActivity;
import co.tiagoaguiar.jchucknorris.jokes.random.component.DaggerJokeComponent;
import co.tiagoaguiar.jchucknorris.jokes.random.component.JokeRepositoryComponent;
import co.tiagoaguiar.jchucknorris.jokes.random.module.JokeModule;
import co.tiagoaguiar.jchucknorris.jokes.random.presenter.JokePresenter;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class JokeActivity extends AbstractActivity {

    public static final String CATEGORY_KEY = "category_key";

    @Inject
    JokePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Bundle args = getIntent().getExtras();
        JokeFragment fragment = (JokeFragment) ActivityUtils.addFragmentToActivity(this, JokeFragment.class);
        fragment.setArguments(args);

        JokeRepositoryComponent jokeRepositoryComponent =
                ((ChuckNorrisApplication) getApplication()).getJokeRepositoryComponent();

        DaggerJokeComponent.builder()
                .jokeRepositoryComponent(jokeRepositoryComponent)
                .jokeModule(new JokeModule(fragment))
                .build()
                .inject(this);


        findViewById(R.id.btn_refresh).setOnClickListener(fragment);
    }

    @Override
    protected Integer getMenuRes() {
        return null;
    }

    @Override
    protected String getToolbarTitle() {
        return getIntent().getExtras().getString(CATEGORY_KEY, super.getToolbarTitle());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.act_joke;
    }

}