package co.tiagoaguiar.jchucknorris.jokes.categories.data.source.remote;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public interface CategoryAPI {

    @GET("jokes/categories")
    Observable<List<String>> findAll();

}