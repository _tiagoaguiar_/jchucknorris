package co.tiagoaguiar.jchucknorris.jokes.random;

import co.tiagoaguiar.jchucknorris.common.view.BaseView;
import co.tiagoaguiar.jchucknorris.jokes.random.view.JokeItem;
import io.reactivex.Observable;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public interface Joke {

    interface View extends BaseView<Presenter> {

        void showJoke(JokeItem jokeItem);

        void showFailure(String message);

    }

    interface Presenter {

        void requestRandomBy(String category);

    }

}
