package co.tiagoaguiar.jchucknorris.jokes.random.data.source;

import javax.inject.Inject;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public class JokeRepository implements JokeDataSource {

    private final JokeDataSource mDataSource;

    @Inject
    public JokeRepository(JokeDataSource dataSource) {
        mDataSource = dataSource;
    }

    @Override
    public void findRandomBy(String category, JokeCallback callback) {
        mDataSource.findRandomBy(category, callback);
    }

}