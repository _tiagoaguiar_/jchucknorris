package co.tiagoaguiar.jchucknorris.jokes.random.component;

import javax.inject.Singleton;

import co.tiagoaguiar.jchucknorris.common.module.NetworkModule;
import co.tiagoaguiar.jchucknorris.jokes.random.data.source.JokeRepository;
import co.tiagoaguiar.jchucknorris.jokes.random.module.JokeRepositoryModule;
import dagger.Component;

/**
 * November, 16 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Singleton
@Component(modules = {JokeRepositoryModule.class, NetworkModule.class})
public interface JokeRepositoryComponent {

    JokeRepository getJokeRepository();

}