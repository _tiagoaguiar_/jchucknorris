package co.tiagoaguiar.jchucknorris.jokes.random.module;

import co.tiagoaguiar.jchucknorris.jokes.random.Joke;
import dagger.Module;
import dagger.Provides;

/**
 * November, 17 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Module
public class JokeModule {

    private final Joke.View mView;

    public JokeModule(Joke.View view) {
        mView = view;
    }

    @Provides
    public Joke.View provideView() {
        return mView;
    }

}