package co.tiagoaguiar.jchucknorris.common.model;

import io.reactivex.disposables.Disposable;

/**
 * November, 17 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public interface DataSource<S, T> {

    void onSuccess(T response);

    void onError(String message);

    void onComplete();

    void onSubscribe(Disposable disposable);

    T transform(S joke);

}