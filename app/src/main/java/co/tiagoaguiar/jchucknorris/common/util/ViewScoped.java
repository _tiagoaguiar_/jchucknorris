package co.tiagoaguiar.jchucknorris.common.util;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
@Documented
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewScoped {
}