package co.tiagoaguiar.jchucknorris.common.view;

import io.reactivex.disposables.Disposable;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public interface BaseView<P> {

    void setPresenter(P presenter);

    void setDisposable(Disposable disposable);

    void showProgressBar();

    void hideProgressBar();

}