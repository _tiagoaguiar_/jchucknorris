package co.tiagoaguiar.jchucknorris.common.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import co.tiagoaguiar.jchucknorris.R;
import io.reactivex.disposables.Disposable;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public abstract class AbstractFragment<P> extends Fragment implements BaseView<P> {

    @Inject
    protected P mPresenter;

    protected Disposable mDisposable;
    protected Context mContext;

    private ProgressDialog mProgressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);

        onCustomCreateView(view, container);

        return view;
    }

    @Override
    public void setPresenter(P presenter) {
        checkNotNull(presenter);
        mPresenter = presenter;
    }

    @Override
    public void setDisposable(Disposable disposable) {
        mDisposable = disposable;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mProgressBar != null) {
            mProgressBar.dismiss();
        }
    }

    @Override
    public void showProgressBar() {
        if (mProgressBar == null) {
            mProgressBar = new ProgressDialog(getContext());
            mProgressBar.setMessage(getString(R.string.loading));
            mProgressBar.setIndeterminate(true);
            mProgressBar.setCancelable(false);
        }
        mProgressBar.show();
    }

    @Override
    public void hideProgressBar() {
        if (mProgressBar != null) {
            mProgressBar.hide();
        }
    }

    protected abstract int getLayoutRes();

    protected abstract void onCustomCreateView(View view, ViewGroup container);

}