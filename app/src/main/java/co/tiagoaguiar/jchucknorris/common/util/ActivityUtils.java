package co.tiagoaguiar.jchucknorris.common.util;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import co.tiagoaguiar.jchucknorris.R;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public final class ActivityUtils {

    public static void setAnimationOnClosed(Activity activity) {
        activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    public static void setAnimationOnOpen(Activity activity) {
        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public static Fragment addFragmentToActivity(@NonNull FragmentActivity activity,
                                                     @NonNull Class<? extends Fragment> clazz) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.content_frame);

        if (fragment == null)
            fragment = Fragment.instantiate(activity, clazz.getName());

        checkNotNull(fragmentManager);
        checkNotNull(fragment);

        if (!fragment.isAdded()) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.content_frame, fragment);
            transaction.commit();
        }
        return fragment;
    }

    private ActivityUtils() {}

}