package co.tiagoaguiar.jchucknorris.common.util;

import android.graphics.Color;

import java.util.Random;

/**
 * November, 17 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public final class Colors {

    public static int randomColor() {
        Random random = new Random();

        int r = random.nextInt(0xFF);
        int g = random.nextInt(0xFF);
        int b = random.nextInt(0xFF);

        int range = 0xA0;

        if (r < range) r = range;
        if (g < range) g = range;
        if (b < range) b = range;

        return Color.argb(0x80, r, g, b);
    }

    private Colors() {}

}