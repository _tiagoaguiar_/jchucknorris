package co.tiagoaguiar.jchucknorris.common.view;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import co.tiagoaguiar.jchucknorris.R;
import co.tiagoaguiar.jchucknorris.common.util.ActivityUtils;

/**
 * November, 15 2018
 *
 * @author tiagoaguiar.ferreira@gmail.com (Tiago Aguiar).
 */
public abstract class AbstractActivity extends AppCompatActivity {

    protected Toolbar mToolbar;

    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutRes());

        mToolbar = findViewById(R.id.toolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle(getToolbarTitle());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Integer menuRes = getMenuRes();
        if (menuRes == null)
            return true;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(getMenuRes(), menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                ActivityUtils.setAnimationOnClosed(this);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    protected String getToolbarTitle() {
        return getString(R.string.app_name);
    }

    protected abstract Integer getMenuRes();

    protected abstract int getLayoutRes();

}