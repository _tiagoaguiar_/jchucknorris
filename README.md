# Aplicativo de Teste - Guia Bolso

## App desenvolvido com:

- Java
- Clean Architecture (MVP)
- RxJava: https://github.com/ReactiveX/RxAndroid
- Retrofit: https://square.github.io/retrofit
- Groupie Recycler: https://github.com/lisawray/groupie
- Picasso: http://square.github.io/picasso
- Dagger2: https://google.github.io/dagger/android
- Design Support Library: https://android-developers.googleblog.com/2015/05/android-design-support-library.html

## Repositories

Repository do projeto Kotlin

- https://bitbucket.org/\_tiagoaguiar\_/chucknorrisio

Repository do projeto na versão Java:

- https://bitbucket.org/\_tiagoaguiar\_/jchucknorris

Apk do Projeto final

- https://www.dropbox.com/s/azlvac557uln6rn/app-debug.apk?dl=0

## Compilação e Execução Pelo Android Studio

Para testar o aplicativo primeiramente você deve instalar o [Android Studio](https://developer.android.com/studio/install?hl=pt-br).

Com o Android Studio instalado e o projeto clonado no seu computador:

`git clone https://bitbucket.org/_tiagoaguiar_/jchucknorris`

Importe o projeto pelo *wizard* do Android Studio: **Open an existing Android Studio project**.

Procure pela pasta **jchucknorris** e clique em Ok.

Clique no botão de **Play** após importar o projeto e escolhe em qual dispositivo o aplicativo deve executar.

## Compilação e Execução Pelo Terminal

Para testar o aplicativo primeiramente você deve instalar o [Android Studio](https://developer.android.com/studio/install?hl=pt-br).

Com o Android Studio instalado e o projeto clonado no seu computador:

`git clone https://bitbucket.org/_tiagoaguiar_/jchucknorris`

Navegue atá a pasta raiz do projeto pelo terminal: 

`cd jchucknorris`

E execute o comando para compilar o projeto:

`./gradlew clean assembleDebug` 

Inicialize um Emulador pelo Android Studio e Execute o comando:

`adb install app/build/outputs/apk/debug/app-debug.apk`

> Importante: o **adb** deve estar no PATH do seu computador.

## Execução Direto Pelo Apk

Baixe o apk pelo link acima.

Segure e arraste o arquivo **app-debug.apk** para dentro do Emulador Android. A instalação irá iniciar automaticamente.

## Notes

> O projeto foi desenvolvido utilizando a arquitetura MVP e organizada da seguinte forma:
>
> 3 pacotes principais:
>
> - Common: Responsável por definir conjuntos de classes compartilhadas na aplicação como Module de requisição HTTP, Annotation de Escopo, Classes Genéricas e etc.
>
> - Jokes: Este  pacote defini os casos de uso sobre "jokes" .Ou seja, para este app em específico temos 2 casos de uso: **Listar Categorias** e **Exibir Piadas Aleatórias de Uma Categoria**
>
> - Splash: Startup do aplicativo. Sem regras de negócio, somente view
>
>
> Nos pacotes dos casos de uso (categories e random), temos a arquitetura MVP + Dagger2 + RxJava
>
> 1. Component: Defini interfaces que gerenciam os módulos da camada de persistência (repository), API Restful e módulo MVP.
> 2. Data.Source (**M**VP):  Conjunto de classes para realizar requisições Remotas (RESTful) e, eventualmente a Local (caso precise persistir dados em um localstorage). Não foi implementado Módulos de persistência.
> 3. Module: Definição dos módulo que fornece objetos para ser "injetados" pelo Dagger2
> 4. Presenter (MV**P**): Camada "Presenter" responsável por decidir que ações tomar após interação do usuário, bem como o que exibir após respostas dos repositories.
> 5. View: (M**V**P): Camada de apresentação dos dados e observers de eventos/interação do usuário com o aplicativo